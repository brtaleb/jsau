const my_shared_code_headless = require('./my_shared_code_headless');
const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const bodyParser = require('body-parser');

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let even_numbers = my_shared_code_headless.generateEvenNumbers(20)
app.get('/oddNumbers', (req,res) => {
  res.write("<h1>");
  even_numbers.forEach(n => {
    res.write(n+" ");
  })
  res.write("</h1>");
  res.end();
})

app.listen(3000, () => console.log("Server listening on port 3000..."));
