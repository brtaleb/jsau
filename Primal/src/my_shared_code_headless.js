'use strict'

function generateEvenNumbers(max) {
  let i = 0;
  let res = [];
  while(i < max){
    if(i % 2 !== 0){
      res.push(i)
    }
    i++;
  }
  return res;
}

module.exports = {
  generateEvenNumbers
}
