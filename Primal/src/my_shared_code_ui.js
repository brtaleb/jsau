'use strict'

var my_shared_code_headless = require('./my_shared_code_headless')

function writeContent() {
  var numbers = my_shared_code_headless.generateEvenNumbers(20);
  let res = "";
  numbers.forEach(n => {
    res.append(n+" ");
  })
  return res;
}

module.exports = {
    writeContent
}
