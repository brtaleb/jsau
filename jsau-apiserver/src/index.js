const express = require('express')
const morgan = require('morgan')
const fs = require('fs')
const bodyParser = require('body-parser')

let app = express()
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get('/nouvelles', (req, res) => {
    fs.readFile('./ressources/nouvelles.json', null, (err, file) => {
        if (err) {
            res.sendStatus(500)
            return
        }
        res.send(file)
        res.end()
    })
})

app.get('/nouvelles/:id', (req, res) => {
    fs.readFile('./ressources/nouvelles.json', null, (err, file) => {
      if (err) {
          res.sendStatus(500)
          return
      }
      let obj = JSON.parse(file)
      let nouvelle = null
      obj.nouvelles.forEach((n) => {
          if (n.id === req.params.id) {
              nouvelle = n
          }
      })

      if (nouvelle) {
          res.send(nouvelle)
          res.end()
      } else {
          res.send('Not Found\n')
          res.end()
      }
    })
})

app.post('/nouvelles', (req, res) => {
    fs.readFile('./ressources/nouvelles.json', null, (err, file) => {
      if (err) {
          res.sendStatus(500)
          return
      }
      let obj = JSON.parse(file)
      obj.nouvelles.push(req.body)

      fs.writeFile('./ressources/nouvelles.json', JSON.stringify(obj), (err) => {
          if (err) {
              console.log(err)
          } else {
              res.send('News added successfully\n')
              res.end()
          }
      })
    })
})

app.put('/nouvelles/:id', (req, res) => {
    fs.readFile('./ressources/nouvelles.json', null, (err, file) => {
      if (err) {
          res.sendStatus(500)
          return
      }
      let obj = JSON.parse(file)

      obj.nouvelles.forEach((n) => {
          if (n.id === req.params.id) {
              n.text = req.body.text
          }
      })

      fs.writeFile('./ressources/nouvelles.json', JSON.stringify(obj), (err) => {
          if (err) {
              console.log(err)
          } else {
              res.send('News ' + req.params.id + ' updated\n')
              res.end()
          }
      })
    })
})

app.delete('/nouvelles/:id', (req, res) => {
    fs.readFile('./ressources/nouvelles.json', null, (err, file) => {
      if (err) {
          res.sendStatus(500)
          return
      }
      let obj = JSON.parse(file)

      obj.nouvelles = obj.nouvelles.filter((n) => {
          return n.id !== req.params.id
      })

      fs.writeFile('./ressources/nouvelles.json', JSON.stringify(obj), (err) => {
          if (err) {
              console.log(err)
          } else {
              res.send('News ' + req.params.id + ' deleted\n')
              res.end()
          }
      })
    })
})

app.listen(3000, () => console.log('Server listening on port 3000...'))
