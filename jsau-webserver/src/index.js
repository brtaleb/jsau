'use strict'
const express = require('express')
const morgan = require('morgan')
const fs = require('fs')
const bodyParser = require('body-parser')
const nunjucks = require('nunjucks')
const path = require('path')
const utils = require('jsau-utils')

let app = express()
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static(path.join()

nunjucks.configure(path.join(__dirname, '/views'), {
    autoescape: true,
    express: app
})

let nouvellesData = []

app.get('/nouvelles', (req, res) => {
    utils.getNouvelles((nouvelles) => {
        if (!req.query.id) {
            nouvellesData = [...nouvelles]
        } else {
            nouvelles.forEach((n) => {
                if (n.id === req.query.id) {
                    nouvellesData = []
                    nouvellesData.push(n)
                }
            })
        }
        res.redirect('/')
    })
})

app.post('/nouvelles', (req, res) => {
    utils.getNouvelles((nouvelles) => {
        const newNouvelle = req.body
        utils.validate(newNouvelle, (existante) => {
            if (existante) {
                console.log('nouvelle existante!')
            } else {
                nouvelles.push(newNouvelle)
                utils.modifierNouvelles(nouvelles)
                nouvellesData = [...nouvelles]
            }
        })
        res.redirect('/')
    })
})


app.post('/delete', (req, res) => {
    utils.getNouvelles((nouvelles) => {
        nouvelles = nouvelles.filter((n) => {
            return n.id !== req.body.id
        })

        fs.writeFile('/home/brahim/JSAU/nouvelles.json', JSON.stringify(nouvelles), (err) => {
            if (err) {
                console.log(err)
            }
        })

        nouvellesData = [...nouvelles]
        res.redirect('/')
    })
})

app.get('/', (req, res) => {
    res.render('index.njk', {nouvellesData})
})

// app.put('/nouvelles/:id', (req,res) => {
//   fs.readFile('./ressources/nouvelles.json', null, (err,file) => {
//     if(err){
//       console.log(err)
//     }
//     else {
//       let obj = JSON.parse(file);
//
//       obj.nouvelles.forEach((n) => {
//         if (n.id === req.params.id){
//           n.text = req.body.text;
//         }
//       });
//
//       fs.writeFile('./ressources/nouvelles.json', JSON.stringify(obj), err => {
//         if(err) console.log(err)
//         else {
//           res.send('News '+req.params.id+' updated\n');
//           res.end();
//         }
//       })
//     }
//   })
// })

app.listen(3000, () => console.log('Server listening on port 3000...'))
